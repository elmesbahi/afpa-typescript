import { log } from "./module1";

class Exemple {
    @log
    maMethodeA() : void{
        console.log('appel methodeA');
    }

    @log
    maMethodeB(param: string) : void{
        console.log('appel methodeB : '+param);
    }
}

let x = new Exemple();
x.maMethodeA();
x.maMethodeB('text');